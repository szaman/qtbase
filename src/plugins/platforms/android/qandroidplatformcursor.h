#ifndef __QANDROIDPLATFORMCURSOR_H_
#define __QANDROIDPLATFORMCURSOR_H_

#include <private/qjni_p.h>
#include <qpa/qplatformcursor.h>

class QAndroidPlatformScreen;

class QAndroidPlatformCursor : public QPlatformCursor
{
public:
    explicit QAndroidPlatformCursor(QAndroidPlatformScreen *screen);

    void changeCursor(QCursor *windowCursor, QWindow *window) override;
    QPoint pos() const override;

private:
    QJNIObjectPrivate m_qtPointer;
    QAndroidPlatformScreen *m_screen;
};

#endif // __QANDROIDPLATFORMCURSOR_H_
